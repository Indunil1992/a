let AWS = require('aws-sdk');
const sns = new AWS.SNS();
const ses = new AWS.SES();
const s3 = new AWS.S3();

exports.handler = function (event, context, callback) {
    s3.listObjects({
        'Bucket': 'indunil1',
        'MaxKeys': 10,
        'Prefix': '2'
    }).promise()
        .then(data => {
            console.log(data);           // successful response
            /*
            data = {
                Contents: [
                    {
                       ETag: "\"70ee1738b6b21e2c8a43f3a5ab0eee71\"",
                       Key: "example1.jpg",
                       LastModified: "<Date Representation>",
                       Owner: {
                          DisplayName: "myname",
                          ID: "12345example25102679df27bb0ae12b3f85be6f290b936c4393484be31bebcc"
                       },
                       Size: 11,
                       StorageClass: "STANDARD"
                    },
                    // {...}
                ]
            }
            */
        })
        .catch(err => {
            console.log(err, err.stack); // an error occurred
        });

    ses.sendEmail({
        Destination: {
            ToAddresses: ['indunil@adroitlogic.com'],
            CcAddresses: [],
            BccAddresses: []
        },
        Message: {
            Body: {
                Text: {
                    Data: 'He is best known as the lead singer and frontman of Queensland alternative rock band Powderfinger. Born and raised in Toowong, Brisbane, he began writing music at 15. With Ian Haug, John Collins, and Darren Middleton, the band released five studio albums in fifteen years and achieved mainstream success in Australia.'
                }
            },
            Subject: {
                Data: 'test'
            }
        },
        Source: 'indunil@adroitlogic.com',
    }, function (err, data) {
        if (err) console.log(err, err.stack); // an error occurred
        else console.log(data);           // successful response
    });

    sns.publish({
        Message: 'sample run',
        Subject: 'subject 1',
        MessageAttributes: {},
        MessageStructure: 'String',
        TopicArn: 'arn:aws:sns:us-east-1:318300609668:TestSNS'
    }).promise()
        .then(data => {
            // your code goes here
        })
        .catch(err => {
            // error handling goes here
        });


    callback(null, { "message": "Successfully executed!" });
}